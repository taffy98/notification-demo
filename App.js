import React, {Component} from 'react';
import {View, StyleSheet, Text} from 'react-native';
import {fcmService} from './services/FCMService';

export default class App extends Component {
  constructor(props) {
    super(props);
  }
  componentDidMount() {
    fcmService.register(
      this.onRegister,
      this.onNotification,
      this.onOpenNotification,
    );
  }
  onRegister(token) {
    console.log('[NotificationFCM] onRegister: ', token);
  }
  onNotification(notify) {
    console.log('[NotificationFCM] onNotification: ', notify);
    const channelObj = {
      channelID: 'SampleChannelID',
      channelName: 'SampleChannelName',
      channelDes: 'SampleChannelDes',
    };
    const channel = fcmService.buildChannel(channelObj);
    const buildNotify = {
      dataId: notify._notificationId,
      title: notify._title,
      content: notify._body,
      sound: 'default',
      channel: channel,
      data: {},
      colorBgIcon: '#1A243B',
      largeIcon: 'ic_launcher',
      vibrate: true,
    };
    const notification = fcmService.buildNotification(buildNotify);
    fcmService.displayNotification(notification);
  }
  onOpenNotification(notify) {
    console.log('[NotificationFCM] onOpenNotification: ', notify);
  }

  render() {
    let {container} = style;
    return (
      <View>
        <Text>Sample React Native FireBase</Text>
      </View>
    );
  }
}
const style = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
});
