/* eslint-disable no-unused-vars */
/* eslint-disable prettier/prettier */
import firebase from 'react-native-firebase';

class FCMService {
  register = (onRegister, onNotification, onOpenNotification) => {
    this.checkPermission(onRegister);
    this.createNotificationListeners(
      onRegister,
      onNotification,
      onOpenNotification,
    );
  };
  checkPermission = onRegister => {
    firebase
      .messaging()
      .hasPermission()
      .then(enabled => {
        if (enabled) {
          // TODO: User has permission
          this.getToken(onRegister);
        } else {
          // TODO: User doesn't have permission
          this.requestPermission(onRegister);
        }
      })
      .catch(error => console.log('Permission reject', error));
  };
  getToken = onRegister => {
    firebase
      .messaging()
      .getToken()
      .then(fcmToken => {
        if (fcmToken) {
          onRegister(fcmToken);
        } else {
          console.log("User doesn't have deviceToken");
        }
      })
      .catch(error => console.log('Get Token rejected', error));
  };
  requestPermission = onRegister => {
    firebase
      .messaging()
      .requestPermission()
      .then(() => {
        this.getToken(onRegister);
      })
      .catch(error => console.log('Request permission reject', error));
  };
  deleteToke = () => {
    firebase
      .messaging()
      .deleteToken()
      .catch(error => console.log('Delete token error', error));
  };
  createNotificationListeners = (
    onRegister,
    onNotification,
    onOpenNotification,
  ) => {
    // Trigger when a particular notification has been received in foreground
    this.notificationListener = firebase
      .notifications()
      .onNotification(notification => {
        onNotification(notification);
      });
    // If your app is in background, you can listen for when a notification is clicked / tapped/ opened as follows
    this.notificationOpenedListener = firebase
      .notifications()
      .onNotificationOpened(notificationOpen => {
        onOpenNotification(notificationOpen);
      });
    // If your app is closed, you can check if it was opened by a notification being clicked / tapped/ opened as follows
    firebase
      .notifications()
      .getInitialNotification()
      .then(notificationOpen => {
        if (notificationOpen) {
          const notification = notificationOpen.notification;
          onOpenNotification(notification);
        }
      });
    // Trigger for data only payload in foreground
    this.messageListener = firebase.messaging().onMessage(message => {
      onNotification(message);
    });
    this.onTokenRefreshListener = firebase
      .messaging()
      .onTokenRefresh(fcmToken => {
        console.log('New refresh token', fcmToken);
        onRegister(fcmToken);
      });
  };
  unRegister = () => {
    this.notificationListener();
    this.notificationOpenedListener();
    this.messageListener();
    this.onTokenRefreshListener();
  };
  buildChannel = obj => {
    console.log(obj);
    return new firebase.notifications.Android.Channel(
      obj.channelID,
      obj.channelName,
      firebase.notifications.Android.Importance.High,
    ).setDescription(obj.channelDes);
  };
  buildNotification = obj => {
    // For Android
    firebase.notifications().android.createChannel(obj.channel);
    // For Android and IOS
    return (
      new firebase.notifications.Notification()
        .setSound(obj.sound)
        .setNotificationId(obj.dataId)
        .setTitle(obj.title)
        .setBody(obj.content)
        .setData(obj.data)
        // For Android
        .android.setChannelId('channelId')
        .android.setLargeIcon(obj.largeIcon) // Create this icon on Android Studio
        .android.setSmallIcon('ic_launcher') // Create this icon on Android Studio
        .android.setColor(obj.colorBgIcon)
        .android.setPriority(firebase.notifications.Android.Priority.High)
        .android.setVibrate(obj.vibrate)
    );
    // .android.setAutoCancel(true) // Auto cancel after received notification
  };
  scheduleNotification = (notification, days, minutes) => {
    const date = new Date();
    if (days) {
      date.setDate(date.getDate() + days);
    }
    if (minutes) {
      date.setMinutes(date.getMinutes() + minutes);
    }
    firebase
      .notifications()
      .scheduleNotification(notification, {fireDate: date.getTime()});
  };
  displayNotification = notification => {
    firebase
      .notifications()
      .displayNotification(notification)
      .catch(error => console.log('Display notification error:', error));
  };
  removeNotification = notification => {
    firebase
      .notifications()
      .removeDeliveredNotification(notification.notificationId);
  };
}
export const fcmService = new FCMService();